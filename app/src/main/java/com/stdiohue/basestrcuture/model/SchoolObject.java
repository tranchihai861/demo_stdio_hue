package com.stdiohue.basestrcuture.model;

/**
 * Created by Van Thien Pro on 7/28/2017.
 */

public class SchoolObject {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SchoolObject(String name) {
        this.name = name;
    }

    public SchoolObject() {
    }
}
