package com.stdiohue.basestrcuture.ui.Activity;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;

import com.stdiohue.basestrcuture.R;
import com.stdiohue.basestrcuture.databinding.ActivitymainBinding;
import com.stdiohue.basestrcuture.model.SchoolObject;
import com.stdiohue.basestrcuture.ui.Adapter.SchoolAdapter;
import com.stdiohue.basestrcuture.utils.LogUtils;
import com.stdiohue.basestrcuture.view.base.BaseActivity;

import java.util.ArrayList;

/**
 * Created by Van Thien Pro on 7/28/2017.
 */

public class MainActivity extends BaseActivity<ActivitymainBinding>{
    public ArrayList<SchoolObject> mListSchool;
    public SchoolAdapter mSchoolAdapter;


    @Override
    protected int getLayoutId() {
        return R.layout.activitymain;
    }

    @Override
    protected void init() {
        data();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        viewDataBinding.rvMain.setLayoutManager(layoutManager);
//        mSchoolAdapter = new SchoolAdapter(this, mListSchool);
        viewDataBinding.rvMain.setAdapter(mSchoolAdapter = new SchoolAdapter(this, mListSchool));
        LogUtils.logE(mListSchool.toString());
    }

    private void data() {
        mListSchool = new ArrayList<>();
        mListSchool.add(new SchoolObject("Trường Đại học Công nghệ Thông tin"));
        mListSchool.add(new SchoolObject("Trường Đại học Khoa học Tự nhiên"));
        mListSchool.add(new SchoolObject("Trường Đại học Khoa học Xã hội và Nhân văn"));
        mListSchool.add(new SchoolObject("Trường Đại học Việt - Đức"));
        mListSchool.add(new SchoolObject("Trường Đại học Công nghệ Thông tin"));
        mListSchool.add(new SchoolObject("Trường Đại học Công nghệ Thông tin"));
        mListSchool.add(new SchoolObject("Trường Đại học Công nghệ Thông tin"));
        mListSchool.add(new SchoolObject("Trường Đại học Công nghệ Thông tin"));
        mListSchool.add(new SchoolObject("Đại học Khoa học Tự nhiên"));
        mListSchool.add(new SchoolObject("Trường Đại học Công nghệ Thông tin"));
        mListSchool.add(new SchoolObject("Trường Đại học Công nghệ Thông tin"));
        mListSchool.add(new SchoolObject("Trường Đại học Việt - Nhật"));
    }

    @Override
    protected void startScreen() {

    }

    @Override
    protected void resumeScreen() {

    }

    @Override
    protected void pauseScreen() {

    }

    @Override
    protected void destroyScreen() {

    }
}
