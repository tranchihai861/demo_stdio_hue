package com.stdiohue.basestrcuture.ui.ViewHolder;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.stdiohue.basestrcuture.R;
import com.stdiohue.basestrcuture.model.SchoolObject;

/**
 * Created by Van Thien Pro on 7/28/2017.
 */

public class SchoolViewHolder extends RecyclerView.ViewHolder {
    private TextView tvname;

    public SchoolViewHolder(View itemView) {
        super(itemView);
        tvname = (TextView) itemView.findViewById(R.id.tv_name);
    }
    public void bind(String school) {
        tvname.setText(school);
    }
}
