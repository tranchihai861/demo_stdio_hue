package com.stdiohue.basestrcuture.ui.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stdiohue.basestrcuture.R;
import com.stdiohue.basestrcuture.model.SchoolObject;
import com.stdiohue.basestrcuture.ui.ViewHolder.SchoolViewHolder;

import java.util.List;

/**
 * Created by Van Thien Pro on 7/28/2017.
 */

public class SchoolAdapter extends RecyclerView.Adapter<SchoolViewHolder>{

    private List<SchoolObject> mListSchool;
    private Context mContext;
    public SchoolAdapter(Context context, List<SchoolObject> item){
        mListSchool = item;
        mContext = context;
    }

    @Override
    public SchoolViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_school, parent, false);
        return new SchoolViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SchoolViewHolder holder, int position) {
        holder.bind(mListSchool.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mListSchool == null ? 0 : mListSchool.size();
    }
}
